#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncore
import asynchat
import socket
import re
import argparse
import sys

__all__ = ["SmtpChannel", "SMTPServer"]
__version__ = 'SMTPot Simple asynchronous mail honeypot 0.1a'

VOID = ''
E354 = '354 End data with <CR><LF>.<CR><LF>'
E451 = '451 Internal confusion'
E500 = '500 Error: bad syntax'
E501 = '501 Syntax: {syntax}'
E502 = '502 Error: command "{command}" not implemented'
E503 = '503 Error: {error}'
OK = '250 Ok'
BYE = '221 Bye'

options = argparse.ArgumentParser(prog='smtpot', description='STMP Honeypot')
options.add_argument('-v', '--verbose', default=0, type=int, metavar='Level')
options.add_argument('--output', default='none')
options.add_argument('-p', '--port', default=10025, type=int, help='Local port to run the server (default: 10025)')
options.add_argument('-l', '--listen-address', metavar='IP', default='127.0.0.1', help='Listen address for the server '
                                                                                       'to bind (default: 127.0.0.1)')


class Debug(object):

    def __init__(self, message, level=options.parse_args(['--output'])):
        self.message = message
        self.level = level.toupper()
        self.debug_level = "Debug_{}".format(self.level)
        self.method = getattr(self, self.debug_level, "Debug_NONE")

    def Debug_STDOUT(self):
        sys.stdout.write(self.message)

    def Debug_STDERR(self):
        sys.stderr.write(self.message)

    def Debug_None(self):
        pass


class SMTPChannel(asynchat.async_chat):

    COMMAND = 0
    DATA = 1
    FAKE = 'ESMTP Postfix (Debian/GNU)'
    FAKE_EHLO = '''250 {fqdn}
250-ETRN
250 8BITMIME'''
    CRLF = bytes('\r\n', encoding='UTF-8')

    def __init__(self, server, conn, addr):
        asynchat.async_chat.__init__(self, conn)
        self.__server = server
        self.__conn = conn
        self.__addr = addr
        self.__rbuffer = [] #Incoming data
        self.__state = self.COMMAND
        self.__greeting = 0
        self.__mailfrom = None
        self.__rcpto = []
        self.__data = ''
        self.__fqdn = socket.getfqdn()  #Set FQDN manually maybe?
        # If socket can't find remote IP address, close connection
        try:
            self.__peer = conn.getpeername()
        except socket.error as err:
            self.close()
        self.push('220 {fqdn} {server}'.format(fqdn=self.__fqdn, server=self.FAKE))
        self.set_terminator(self.CRLF)

    def push(self, msg):
        asynchat.async_chat.push(self, bytes(msg + '\r\n', encoding='UTF-8'))

    def collect_incoming_data(self, data):
        self.__rbuffer.append(data.decode())

    def found_terminator(self):
        rbuffer = VOID.join(self.__rbuffer)
        self.__rbuffer = []
        #Reset incoming buffer after assigned to another variable
        if self.__state == self.COMMAND:
            if not rbuffer:
                self.push(E500)
                return
            method = None
            i = rbuffer.find(' ')
            if i < 0:
                command = rbuffer.upper()
                arg = None
            else:
                command = rbuffer[:i].upper()
                arg = rbuffer[i+1:].strip()
            method = getattr(self, 'smtp_{}'.format(command), None)
            if not method:
                self.push(E502.format(command=command))
                return
            method(arg)
            return
        else:
            if self.__state != self.DATA:
                self.push(E451)
                return
            # Server msg processing is only for logging purposes
            status = self.__server.process_message(self.__peer,
                                                   self.__mailfrom,
                                                   self.__rcpto,
                                                   self.__data #Mail data is ignored
                                                   )
            self.__rcpto = []
            self.__mailfrom = None
            self.__state = self.COMMAND
            self.set_terminator(self.CRLF)
            if not status:
                self.push(OK)
            else:
                self.push(status)

    def smtp_HELO(self, arg):
        if not arg:
            Debug('Sent {err} to {peer}'.format(err=E501.format(syntax='HELO hostname')), peer=self.__peer)
            self.push(E501.format(syntax='HELO hostname'))
            return
        if self.__greeting:
            self.push(E503.format(error='Duplicate HELO'))
        else:
            self.__greeting = arg
            self.push('250 {fqdn}'.format(fqdn=self.__fqdn))

    def smtp_EHLO(self, arg):
        if not arg:
            self.push(E501.format(syntax='EHLO hostname'))
            return
        if self.__greeting:
            self.push(E503.format(error='Duplicate EHLO'))
        else:
            self.__greeting = arg
            self.push(self.FAKE_EHLO.format(fqdn=self.__fqdn))

    def smtp_NOOP(self, arg):
        if arg:
            self.push(E501.format(syntax='NOOP '))
        else:
            self.push(OK)

    def smtp_QUIT(self, arg):
        #arg ignored
        self.push(BYE)
        self.close_when_done()

    def __getemail(self, route, arg):
        match = re.search(r'[\w.-]+@[\w.-]+.\w+', arg[len(route):])
        if match:
            return match.group(0)
        else:
            return None

    def smtp_MAIL(self, arg):
        address = self.__getemail('FROM:', arg)
        if not address:
            self.push(E501.format(syntax='MAIL FROM:<address>'))
        elif self.__mailfrom:
            self.push(E503.format(error='nested MAIL command'))
            return
        else:
            self.__mailfrom = address
            self.push(OK)

    def smtp_RCPT(self, arg):
        if not self.__mailfrom:
            self.push(E503.format(error='need MAIL command'))
            return
        address = self.__getemail('TO:', arg)
        if not address:
            self.push(E501.format(syntax='RCPT TO: <address>'))
            return
        self.__rcpto.append(address)
        self.push(OK)

    def smtp_RSET(self, arg):
        if arg:
            self.push(E501.format(syntax='RSET'))
            return
        # Reset everything but the greeting
        self.__mailfrom = None
        self.__rcpto = []
        self.__data = ''
        self.__state = self.COMMAND
        self.push(OK)

    def smtp_DATA(self, arg):
        if not self.__rcpto:
            self.push(E503.format(error='need RCPT command'))
            return
        if arg:
            self.push(E501.format(syntax='DATA'))
            return
        self.__state = self.DATA
        self.set_terminator(bytes('{0}.{0}'.format('\r\n'), encoding='UTF-8'))
        self.push(E354)


class SMTPServer(asyncore.dispatcher):

    def __init__(self, localaddr, localport):
        self._localaddr = localaddr
        self._localport = localport
        asyncore.dispatcher.__init__(self)
        try:
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            self.set_reuse_addr()
            self.bind(('', self._localport))
            self.listen(5)
        except PermissionError:
            self.close()
            print('Shit must be run as root')
        else:
            print('Server started on port {port}'.format(port=self._localport))

    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            conn, addr = pair
            channel = SMTPChannel(self, conn, addr)

    def process_message(self, peer, mailfrom, rcpto, data):
        #Work in progress, add features to do with the intruder

        print('{} with {} sends {} '.format(peer, mailfrom, rcpto))


if __name__ == '__main__':
    args = options.parse_args()
    smtpot = SMTPServer('127.0.0.1', 10025)
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        print('Signal received. Closing...')