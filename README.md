# SMTPot #

Simple asynchronous mail honeypot based on Python3 and the asyncore/asynchat native libraries.

This is a work in progress.

### Features

Feature                    |   Status
:--------------------------|------------------:
Set secure default options | In Progress
Define levels of verbosity | To be done
Setup a web interface for remote management | To be done
Define an statistics model  | To be done
Setup a basic data logging system | To be done
Handle firewall blacklisting | To be done